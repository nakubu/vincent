class Artwork < ApplicationRecord
  belongs_to :artist
  acts_as_taggable_on :mediums
  validates :title, :image, :date, :medium_list, presence: true
end
