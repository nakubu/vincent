class Artist < ApplicationRecord
  validates :name, :image, presence: true
end
