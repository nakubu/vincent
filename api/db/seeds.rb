# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Artist.create({name: 'Artist 1', image: '/static/artist1.jpg'})
Artist.create({name: 'Artist 2', image: '/static/artist2.jpg'})
Artist.create({name: 'Artist 3', image: '/static/artist3.jpg'})
Artist.create({name: 'Artist 4', image: '/static/artist4.jpg'})

4.times do
  Artwork.create({
    title: 'Artwork 1',
    image: '/static/artwork1.jpg',
    artist_id: 1,
    date: '2017-05-01',
    medium_list: ['Cob', 'Glass'],
    active: true
  })

  Artwork.create({
    title: 'Artwork 2',
    image: '/static/artwork2.jpg',
    artist_id: 2,
    date: '2017-05-02',
    medium_list: ['Glass', 'Metal'],
    active: false
  })

  Artwork.create({
    title: 'Artwork 3',
    image: '/static/artwork3.jpg',
    artist_id: 3,
    date: '2017-05-03',
    medium_list: ['Metal', 'Stone'],
    active: true
  })

  Artwork.create({
    title: 'Artwork 4',
    image: '/static/artwork4.jpg',
    artist_id: 4,
    date: '2017-05-04',
    medium_list: ['Stone', 'Wood'],
    active: false
  })
end
