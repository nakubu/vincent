# Vincent

> Artwork management application

This is a merged version of two separate repos for [API Server](https://bitbucket.org/nakubu/vincent-api) and [Web App](https://bitbucket.org/nakubu/vincent-web) parts.

## API server Setup

``` bash
# go to server folder
cd ./api

# install dependencies
bundle

# create and seed database
rake db:create db:migrate db:seed

# serve at localhost:3000
rails s
```

## Web application Setup

``` bash
# go to app folder
cd ./web

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# run e2e tests
npm run e2e
```
