// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.css';
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';
import app from './app';
import router from './router';

Vue.config.productionTip = false;
Vue.use(VueMaterial);
Vue.use(VueResource);
Vue.use(VeeValidate);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<app/>',
  components: {app}
});
