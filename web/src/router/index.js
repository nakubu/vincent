import Vue from 'vue';
import Router from 'vue-router';
import home from '@/components/home';
import artistIndex from '@/components/artist';
import artistShow from '@/components/artist/show';
import artworkIndex from '@/components/artwork';
import artworkShow from '@/components/artwork/show';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [{
      path: '/',
      meta: {title: 'Artworks'},
      component: artworkIndex
    }, {
      path: '/artists',
      meta: {title: 'Artists'},
      component: artistIndex
    }, {
      path: '/artists/add',
      meta: {title: 'Create Artist'},
      component: artistShow
    }, {
      path: '/artists/:id',
      meta: {title: 'Edit Artist'},
      component: artistShow
    }, {
      path: '/artworks',
      meta: {title: 'Artworks'},
      component: artworkIndex
    }, {
      path: '/artworks/add',
      meta: {title: 'Create Artwork'},
      component: artworkShow
    }, {
      path: '/artworks/:id',
      meta: {title: 'Edit Artwork'},
      component: artworkShow
    }]
});
