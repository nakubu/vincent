module.exports = {
  'artist form validation e2e tests': function (browser) {
    var url = browser.globals.devServerURL + '/artists/add'
    browser
      .url(url)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.artist-form')
      .click('button[type=submit]')
      .pause(1000)
      .assert.elementCount('.md-input-invalid', 2)
      .setValue('input[data-vv-name=name]', 'Artist Name')
      .setValue('input[data-vv-name=image]', '/static/artist1.jpg')
      .pause(1000)
      .assert.elementCount('.md-input-invalid', 0)
      .click('button[type=submit]')
      .pause(1000)
      .assert.elementPresent('.artist-list')
      .end()
  },

  'artwork form validation e2e tests': function (browser) {
    var url = browser.globals.devServerURL + '/artworks/add'
    browser
      .url(url)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('.artwork-form')
      .click('button[type=submit]')
      .pause(1000)
      .assert.elementCount('.md-input-invalid', 5)
      .setValue('[data-vv-name=title]', 'Artwork Title')
      .setValue('[data-vv-name=image]', '/static/artist1.jpg')
      .click('[data-vv-name=artist]')
      .click('.md-select-content .md-list li:first-child')
      .pause(1000)
      .click('[data-vv-name=date] input')
      .click('[data-vv-name=date] .day[track-by=timestamp]')
      .pause(1000)
      .setValue('[data-vv-name=medium_list] input', 'oil')
      .keys(browser.Keys.ENTER)
      .pause(1000)
      .assert.elementCount('.md-input-invalid', 0)
      .click('button[type=submit]')
      .pause(1000)
      .assert.elementPresent('.artwork-list')
      .end()
  }
}
